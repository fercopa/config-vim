set shell=/usr/bin/zsh
let &t_Co = 256
" for vim 8
set termguicolors
set fileencoding=utf-8
set encoding=utf-8
set nowrap
set cc=120
" Leader
let mapleader = ","
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set nu
set mouse=a
set scrolloff=3
set clipboard=unnamedplus

" Folding
set foldnestmax=2
set foldmethod=indent
set nofoldenable

" remove ugly vertical lines on window division
set fillchars+=vert:\ 

let fancy_symbols_enabled = 1


colorscheme jellybeans
" use 256 colors when possible
let g:jellybeans_overrides = {
\   'background': { 'guibg': '000000' },
\   'String': { 'guifg': '58ff74' },
\   'Function': { 'guifg': 'f98921' },
\}
let g:purify_override_colors = {'pink':  { 'gui': '#33ff33', 'cterm': '150' }}

" autocompletion of files and commands behaves like shell
" (complete only the common part, list the options that match)
set wildmode=list:longest
" clear empty spaces at the end of lines on save of python files
autocmd BufWritePre *.py :%s/\s\+$//e

" save as sudo
ca w!! w !sudo tee "%"

au FileType python map <silent> <leader>b Oimport ipdb; ipdb.set_trace()<esc>
autocmd FileType html setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType htmldjango setlocal shiftwidth=2 tabstop=2 softtabstop=2
autocmd FileType javascript setlocal shiftwidth=2 tabstop=2 softtabstop=2

" highlight ColorColumn ctermbg=236
highlight ColorColumn guibg=#5e5e5e

" don;t show these file types
let NERDTreeIgnore = ['\.pyc$', '\.pyo$']

" Fix directory colors
highlight! link NERDTreeFlags NERDTreeDir
" Remove expandable arrow
let g:WebDevIconsNerdTreeBeforeGlyphPadding = ""
let g:WebDevIconsUnicodeDecorateFolderNodes = v:true
let NERDTreeDirArrowExpandable = "\u00a0"
let NERDTreeDirArrowCollapsible = "\u00a0"
let NERDTreeNodeDelimiter = "\x07"
let g:NERDTreeMouseMode=3
" Autorefresh on tree focus
function! NERDTreeRefresh()
    if &filetype == "nerdtree"
        silent exe substitute(mapcheck("R"), "<CR>", "", "")
    endif
endfunction
autocmd BufEnter * call NERDTreeRefresh()
" show big letters
let g:choosewin_overlay_enable = 1
" Signify ------------------------------
" UPDATE it to reflect your preferences, it will speed up opening files
let g:signify_vcs_list = ['git', 'hg']


" Fix to let ESC work as espected with Autoclose plugin
" (without this, when showing an autocompletion window, ESC won't leave insert
"  mode)
let g:AutoClosePumvisible = {"ENTER": "\<C-Y>", "ESC": "\<ESC>"}

" Yankring -------------------------------
let g:yankring_history_dir = '~/.config/nvim/'
" Fix for yankring and neovim problem when system has non-text things
" copied in clipboard
let g:yankring_clipboard_monitor = 0

" Airline ------------------------------
let g:airline#extensions#virtualenv#enabled = 1
let g:airline_theme = 'serene'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'
let g:airline#extensions#fzf#enabled = 1
let g:airline_powerline_fonts = 0
let g:airline#extensions#whitespace#enabled = 0

if fancy_symbols_enabled
    let g:webdevicons_enable = 1

    " custom airline symbols
    " if !exists('g:airline_symbols')
    "    let g:airline_symbols = {}
    " endif
    " let g:airline_left_sep = ''
    " let g:airline_left_sep = ''
    " let g:airline_right_sep = ''
    " let g:airline_right_sep = ''
    " let g:airline_symbols.crypt = '🔒'
    " let g:airline_symbols.linenr = '␊'
    " let g:airline_symbols.linenr = '␤'
    " let g:airline_symbols.linenr = '¶'
    " let g:airline_symbols.maxlinenr = '☰'
    " let g:airline_symbols.maxlinenr = ''
    " let g:airline_symbols.branch = '⎇'
    " let g:airline_symbols.paste = 'ρ'
    " let g:airline_symbols.paste = 'Þ'
    " let g:airline_symbols.paste = '∥'
    " let g:airline_symbols.spell = 'Ꞩ'
    " let g:airline_symbols.notexists = '∄'
    " let g:airline_symbols.whitespace = 'Ξ'

    " let g:airline_left_sep = ''
    " let g:airline_left_alt_sep = ''
    " let g:airline_right_sep = ''
    " let g:airline_right_alt_sep = ''
    " let g:airline_symbols.branch = '⭠'
    " let g:airline_symbols.readonly = '⭤'
    " let g:airline_symbols.linenr = '⭡'
else
    let g:webdevicons_enable = 0
endif

" Custom configurations ----------------
let g:ale_linters_explicit = 1
let g:ale_set_signs = 1
let g:ale_linters = {'python': ['flake8', 'bandit', 'pylint']}
let g:ale_fixers = {'*': [], 'python': ['black', 'isort']}
" let g:ale_linters_ignore = {'python': ['pylint']}
let g:ale_linters_ignore = ['pylint']
let g:ale_fix_on_save = 0
let g:ale_sign_error = '•'
let g:ale_sign_warning = '•'
highlight ALEErrorSign ctermfg=9 guifg=#fd4949
highlight ALEWarningSign ctermfg=11 guifg=#ffd242

let g:flake8_show_in_file=1  " show
let g:flake8_show_in_gutter=1  " show
let g:gitgutter_preview_win_floating = 1

" Configuration example
let g:floaterm_keymap_prev   = '<F8>'
let g:floaterm_keymap_next   = '<F9>'
let g:floaterm_keymap_toggle = '<F12>'
hi Floaterm guibg=black
let g:floaterm_wintype = 'floating'

set cursorline
hi cursorline cterm=none term=none
autocmd WinEnter * setlocal cursorline
autocmd WinLeave * setlocal nocursorline
highlight clear CursorLine
highlight CursorLine guibg=#202021 ctermbg=234
hi Search guibg=#044165 guifg=white

set background=dark
filetype plugin indent on
