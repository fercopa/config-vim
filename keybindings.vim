map tt :tabnew 
map tn :tabn<CR>
map tp :tabp<CR>
" clear search results
nnoremap <silent> // :noh<CR>
" toggle nerdtree display
map <F3> :NERDTreeToggle<CR>
" open nerdtree with the current file selected
nmap <leader>t :NERDTreeFind<CR>

" Fzf ------------------------------
" file finder mapping
nmap <leader>ff :Files<CR>
" tags (symbols) in current file finder mapping
nmap <leader>g :BTag<CR>
" the same, but with the word under the cursor pre filled
nmap <leader>wg :execute ":BTag " . expand('<cword>')<CR>
nmap <leader>l :Ag<CR>
nmap <leader>fw :execute ":Ag " . expand('<cword>')<CR>
" tags (symbols) in all files finder mapping
nmap <leader>G :Tags<CR>
" the same, but with the word under the cursor pre filled
nmap <leader>wG :execute ":Tags " . expand('<cword>')<CR>
" general code finder in current file mapping
nmap <leader>f :BLines<CR>
" the same, but with the word under the cursor pre filled
nmap <leader>wf :execute ":BLines " . expand('<cword>')<CR>
" general code finder in all files mapping
nmap <leader>F :Lines<CR>
" the same, but with the word under the cursor pre filled
nmap <leader>wF :execute ":Lines " . expand('<cword>')<CR>
" commands finder mapping
nmap <leader>c :Commands<CR>
" Ack.vim ------------------------------
" mappings
nmap <leader>r :Ack 
" mapping
nmap  -  <Plug>(choosewin)
" mappings to jump to changed blocks
nmap <leader>sn <Plug>(GitGutterNextHunk)
nmap <leader>sp <Plug>(GitGutterPrevHunk)

nmap <F7> :FloatermNew ipython<CR>

" Ale Warning-errors
map <F5> :ALEPrevious<CR>
map <F6> :ALENext<CR>
