let vim_plug_path = expand('~/.config/nvim/autoload/plug.vim')
let vim_plug_just_installed = 0

if !filereadable(vim_plug_path)
    echo "Installing Vim-plug..."
    echo ""
    silent !mkdir -p ~/.config/nvim/autoload
    silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    let vim_plug_just_installed = 1
endif
" manually load vim-plug the first time
if vim_plug_just_installed
    :execute 'source '.fnameescape(vim_plug_path)
endif

" this needs to be here, so vim-plug knows we are declaring the plugins we
" want to use
call plug#begin("~/.config/nvim/plugged")
" Now the actual plugins:
Plug 'kyoz/purify', { 'rtp': 'vim' }
" Override configs by directory
Plug 'arielrossanigo/dir-configs-override.vim'
" Better file browser
Plug 'preservim/nerdtree'
" Search results counter
Plug 'vim-scripts/IndexedSearch'
Plug 'patstockwell/vim-monokai-tasty'
" Airline
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" Code and files fuzzy finder
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
" Completion from other opened files
Plug 'Shougo/context_filetype.vim'
" Automatically close parenthesis, etc
Plug 'Townk/vim-autoclose'
" Surround
Plug 'tpope/vim-surround'
" Indent text object
Plug 'michaeljsmith/vim-indent-object'
" Indentation based movements
Plug 'jeetsukumaran/vim-indentwise'
" Better language packs
Plug 'sheerun/vim-polyglot'
" Ack code search (requires ack installed in the system)
Plug 'mileszs/ack.vim'
" Highlight matching html tags
Plug 'valloric/MatchTagAlways'
" Generate html in a simple way
Plug 'mattn/emmet-vim'
" Git integration
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'
" Yank history navigation
Plug 'vim-scripts/YankRing.vim'
Plug 'voldikss/vim-floaterm'
Plug 'justinmk/vim-sneak'
" Nice icons in the file explorer and file type status line.
Plug 'ryanoasis/vim-devicons'
Plug 'psf/black', { 'tag': '19.10b0' }
Plug 'dense-analysis/ale'
" Consoles as buffers (neovim has its own consoles as buffers)
Plug 'rosenfeld/conque-term'
" XML/HTML tags navigation (neovim has its own)
Plug 'vim-scripts/matchit.zip'
Plug 'jmcantrell/vim-virtualenv'

Plug 'tpope/vim-repeat'
" Tell vim-plug we finished declaring plugins, so it can load them
call plug#end()
if vim_plug_just_installed
    echo "Installing Bundles, please ignore key map error messages"
    :PlugInstall
endif
